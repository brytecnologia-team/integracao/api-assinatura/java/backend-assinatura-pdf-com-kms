package br.com.bry.framework.exemplo.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;
import org.springframework.web.server.ResponseStatusException;

import br.com.bry.framework.exemplo.configuration.ServiceConfiguration;
import br.com.bry.framework.exemplo.util.UUIDChave;

@Service
public class AssinadorKMSService {
	@Autowired
	private RestTemplate restTemplate;

	public String assinar(HttpServletRequest request) throws JSONException {

		ResponseEntity<String> responseInitialize = null;

		//Cria requisição para enviar ao servidor do HUB
		System.out.println("Parseando requisicao recebida do front");
		HttpEntity<?> requestToAPI = this.getHttpEntity(request);

		//Envia requisição ao HUB
		System.out.println("Enviando requisição ao HUB2");
		try {
		responseInitialize = restTemplate
				.exchange(ServiceConfiguration.URI_HUB + "/fw/v1/pdf/kms/lote/assinaturas", HttpMethod.POST, requestToAPI, String.class);
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			System.out.println("Mensagem de erro recebida: " + e.getResponseBodyAsString());
			JSONObject json = new JSONObject(e.getResponseBodyAsString());
			
			throw new ResponseStatusException(e.getStatusCode(), json.getString("message"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Não foi possível decodificar mensagem de erro recebida da API.");
		}

		//Obtem url de download do documento assinado para retornar ao front
		System.out.println("Obtendo link de download do documento assinado para retornar ao frontend");
		JSONObject body = new JSONObject(responseInitialize.getBody());
		return body.getJSONArray("documentos").getJSONObject(0).getJSONArray("links").getJSONObject(0).getString("href");

	}

	private HttpEntity<?> getHttpEntity(HttpServletRequest request) throws JSONException {

		List<MultipartFile> currentDocumentStreamContentValue = ((StandardMultipartHttpServletRequest) request).getMultiFileMap()
				.get("documento");

		Resource resourceOriginalDocument = null;

		//Obtem o documento PDF
		if (currentDocumentStreamContentValue != null && !currentDocumentStreamContentValue.isEmpty()) {
			resourceOriginalDocument = currentDocumentStreamContentValue.get(0).getResource();
		}

		Resource resourceImage = null;

		List<MultipartFile> currentImageStreamContentValue = ((StandardMultipartHttpServletRequest) request).getMultiFileMap()
				.get("imagem");
		//Obtem imagem caso foi enviada
		if (currentImageStreamContentValue != null && !currentImageStreamContentValue.isEmpty()) {
			resourceImage = currentImageStreamContentValue.get(0).getResource();
		}

		JSONObject dadosAssinatura = new JSONObject();
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

		//Configura JSON dados_assinatura
		dadosAssinatura.put("perfil", request.getParameterValues("perfil")[0]);
		dadosAssinatura.put("algoritmoHash", request.getParameterValues("algoritmoHash")[0]);
		
		//Adiciona o documento na requisicao
		map.add("documento", resourceOriginalDocument);

		//Configura header da requisição
		final HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", ServiceConfiguration.AUTHORIZATION); // Token de acesso (access_token), obtido no BRyCloud 
		//(https://api-assinatura.bry.com.br/api-assinatura-digital#autenticacao-apis). Para uso corporativo, o
		//token deve ser da pessoa juridica e para uso pessoal, da pessoa fisica.
	
		//Configurando as credenciais do KMS BRy (Ou DINAMO/GOVBR)
		
		String kmsType = request.getParameterValues("kms_type")[0];
		headers.set("kms_type", kmsType); //Available values : BRYKMS, GOVBR, DINAMO

		JSONObject kmsData = new JSONObject();
		
		String kmsTypeSistema = request.getParameterValues("kms_type_credential")[0];
		
		//Valor da Credencial
		switch (kmsTypeSistema) {
		case "PIN":
			kmsData.put("pin", request.getParameterValues("valor_credencial")[0]);
			break;
		case "TOKEN":
			kmsData.put("token", request.getParameterValues("valor_credencial")[0]);
			break;
		case "OTP":
			kmsData.put("pin", request.getParameterValues("valor_credencial")[0]);
			break;	
		}		

		//UUID do Certificado no KMS BRy / DINAMO
		if (request.getParameterValues("uuid_cert") != null)
			kmsData.put("uuid_cert", request.getParameterValues("uuid_cert")[0]);
		//CPF do usuario do KMS ou nome de usuário (login) do DINAMO
		if (request.getParameterValues("user") != null)
			kmsData.put("user", request.getParameterValues("user")[0]);
		//UUID da chave privada do certificado caso seja DINAMO
		if (kmsType.equals("DINAMO"))
			if (request.getParameterValues("uuid_cert") != null)
				kmsData.put("uuid_pkey", request.getParameterValues("uuid_pkey")[0]);

		dadosAssinatura.put("kms_data", kmsData);
		map.add("dados_assinatura", dadosAssinatura.toString());
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		//Configura assinatura visivel
		if (request.getParameterValues("assinaturaVisivel")[0].equals("true")) {
			JSONObject imageConfiguration = new JSONObject();
			imageConfiguration.put("altura", request.getParameterValues("altura")[0]);
			imageConfiguration.put("largura", request.getParameterValues("largura")[0]);
			imageConfiguration.put("posicao", request.getParameterValues("posicao")[0]);
			imageConfiguration.put("pagina", request.getParameterValues("pagina")[0]);
			map.add("configuracao_imagem", imageConfiguration.toString());

			JSONObject textConfiguration = new JSONObject();
			if (request.getParameterValues("incluirTXT")[0].equals("true")) {
				textConfiguration.put("texto", request.getParameterValues("texto")[0]);
			}
			textConfiguration.put("incluirCPF", request.getParameterValues("incluirCPF")[0]);
			textConfiguration.put("incluirCN", request.getParameterValues("incluirCN")[0]);
			textConfiguration.put("incluirEmail", request.getParameterValues("incluirEmail")[0]);
			map.add("configuracao_texto", textConfiguration.toString());

		}
		//Adiciona imagem na requisicao caso for enviada
		if (resourceImage != null) {
			map.add("imagem", resourceImage);
		}
		System.out.println("Requisição para o HUB2 criada");

		return new HttpEntity<>(map, headers);
	}

}
