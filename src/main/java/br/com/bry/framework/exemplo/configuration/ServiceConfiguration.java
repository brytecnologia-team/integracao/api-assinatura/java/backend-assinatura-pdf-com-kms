package br.com.bry.framework.exemplo.configuration;

public class ServiceConfiguration {
	
	public static final String AUTHORIZATION = ""; 
	
    // Endereço de acesso ao HUB-Signer
    // Endereço de produção: https://hub2.bry.com.br
    // Endereço de homologação: https://hub2.hom.bry.com.br
	public static final String URI_HUB = "";
	
}
